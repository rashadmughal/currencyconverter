package uk.co.seamlesssolutions.clients.dlapiper.request;

import java.util.Currency;

public class CurrencyConversionRequest implements IConversionRequest {

    private double amount;
    private Currency from;
    private Currency to;
    
    public CurrencyConversionRequest(double amount, Currency from, Currency to) {
        this.amount = amount;
        this.from = from;
        this.to = to;
    }

    public double getAmount() {
        return amount;
    }

    public Currency getFrom() {
        return from;
    }

    public Currency getTo() {
        return to;
    }
}
