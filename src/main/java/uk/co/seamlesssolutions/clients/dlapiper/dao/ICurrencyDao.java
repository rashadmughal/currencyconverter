package uk.co.seamlesssolutions.clients.dlapiper.dao;

import java.io.Serializable;
import java.util.Currency;

public interface ICurrencyDao extends Serializable {

    String getCountry();

    Currency getCurrencyCode();

    String getCurrencyName();

    double getValueInGbp();
}
