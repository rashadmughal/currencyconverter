package uk.co.seamlesssolutions.clients.dlapiper.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;

import uk.co.seamlesssolutions.clients.dlapiper.dao.CurrencyDaoImpl;
import uk.co.seamlesssolutions.clients.dlapiper.dao.ICurrencyDao;

public class CurrencyLoader {
    
    // Should use a library here
    // There is no error checking here as you can see!
    public Map<Currency, ICurrencyDao> fromCsv(File csvFile) {
        Map<Currency, ICurrencyDao> result = new HashMap<>();
        
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(csvFile));
            String line;
            while ((line = br.readLine()) != null) {
                String[] lineSplit = line.split(",");
                
                String country = lineSplit[0];
                String name = lineSplit[1];
                Currency code = Currency.getInstance(lineSplit[2]);
                double rate = Double.parseDouble(lineSplit[3]);
                result.put(code, new CurrencyDaoImpl(country, code, name, rate));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
