package uk.co.seamlesssolutions.clients.dlapiper.exceptions;

public class UnknownCurrencyException extends Exception {

     /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public UnknownCurrencyException(String msg) {
         super(msg);
     }
}
