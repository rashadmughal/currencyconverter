package uk.co.seamlesssolutions.clients.dlapiper.commandline;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Currency;

import uk.co.seamlesssolutions.clients.dlapiper.request.CurrencyConversionRequest;
import uk.co.seamlesssolutions.clients.dlapiper.request.IConversionRequest;

public class ConsoleParser {

    public static IConversionRequest commandLineReader() {
        
        //Console c = System.console();
        
        BufferedReader c = new BufferedReader(new InputStreamReader(System.in));
        
        if (c == null) {
            System.err.println("No console.");
            System.exit(1);
        }
        
        double amount = readAmount(c, "Enter a currency in the following formate X.XX: ");
        
        Currency from = readCurrency(c, "Enter the source currency code (e.g. AUD): ");
        
        Currency to = readCurrency(c, "Enter the destination currency code (e.g. AED): ");
        
        return new CurrencyConversionRequest(amount, from, to);
    }
    
    private static Currency readCurrency(BufferedReader c, String msg) {
        while (true) {
            try  {
                System.out.println(msg);
                String currency = c.readLine();// readLine(false);
                Currency isoCurrency = Currency.getInstance(currency);
                return isoCurrency;
            } catch (IllegalArgumentException | IOException e) {
                System.out.println("That is not a valid currency code please try again");
            }
        }
    }
    
    private static double readAmount(BufferedReader c, String msg) {
        while (true) {
            try  {
                System.out.println(msg);
                String currency = c.readLine();
                return Double.parseDouble(currency);
            } catch (NullPointerException | NumberFormatException | IOException e) {
                System.out.println("That is not a valid amount code please try again:");
            }
        }
    }
}
