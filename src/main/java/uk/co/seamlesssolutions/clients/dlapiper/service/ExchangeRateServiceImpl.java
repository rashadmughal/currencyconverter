package uk.co.seamlesssolutions.clients.dlapiper.service;

import java.util.Currency;
import java.util.Map;

import uk.co.seamlesssolutions.clients.dlapiper.dao.ICurrencyDao;
import uk.co.seamlesssolutions.clients.dlapiper.exceptions.UnknownCurrencyException;

public class ExchangeRateServiceImpl implements IExchangeRateService {
    
    private Map<Currency, ICurrencyDao> curencyMap;

    public ExchangeRateServiceImpl(Map<Currency, ICurrencyDao> currencyMap) {
        this.curencyMap = currencyMap;
    }
    
    @Override
    public ICurrencyDao getCurrency(Currency currencyCode) throws UnknownCurrencyException {
        if (this.curencyMap.containsKey(currencyCode)) {
            return this.curencyMap.get(currencyCode);
        }
        throw new UnknownCurrencyException(String.format("Unrecognised currency code %s", currencyCode));
    }
    
}
