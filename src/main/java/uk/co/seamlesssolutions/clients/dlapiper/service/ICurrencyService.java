package uk.co.seamlesssolutions.clients.dlapiper.service;

import uk.co.seamlesssolutions.clients.dlapiper.dao.ICurrencyDao;

public interface ICurrencyService {

    double convert(double amount, ICurrencyDao from, ICurrencyDao to);
}
