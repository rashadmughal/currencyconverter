package uk.co.seamlesssolutions.clients.dlapiper;

import java.io.File;
import java.util.Currency;
import java.util.Map;

import uk.co.seamlesssolutions.clients.dlapiper.commandline.ConsoleParser;
import uk.co.seamlesssolutions.clients.dlapiper.dao.ICurrencyDao;
import uk.co.seamlesssolutions.clients.dlapiper.exceptions.UnknownCurrencyException;
import uk.co.seamlesssolutions.clients.dlapiper.request.IConversionRequest;
import uk.co.seamlesssolutions.clients.dlapiper.service.CurrencyServiceImpl;
import uk.co.seamlesssolutions.clients.dlapiper.service.ExchangeRateServiceImpl;
import uk.co.seamlesssolutions.clients.dlapiper.service.ICurrencyService;
import uk.co.seamlesssolutions.clients.dlapiper.service.IExchangeRateService;
import uk.co.seamlesssolutions.clients.dlapiper.util.CurrencyLoader;

public class App {
    
    /*
     * There are 0 dependencies on external libraries, hence manual csv parsing and no DI.
     * Simply import and run via an IDE.
     * 
     */
    public static void main(String[] args){
        
        Map<Currency, ICurrencyDao> map =  new CurrencyLoader().fromCsv(new File("src/main/resources/source.csv"));
        IExchangeRateService exchange = new ExchangeRateServiceImpl(map);
        ICurrencyService service = new CurrencyServiceImpl();
        
        IConversionRequest request = ConsoleParser.commandLineReader();
        
        try {
            ICurrencyDao from = exchange.getCurrency(request.getFrom());
            ICurrencyDao to = exchange.getCurrency(request.getTo());
            double toCcy =  service.convert(request.getAmount(), from, to);
            double toCcyRounded = Math.round(toCcy * 100.0) / 100.0;
            System.out.println(String.format("Converted amount is: %s in currency: %s and country: %s", toCcyRounded,                     
                    to.getCurrencyName(), to.getCountry()));
        } catch (UnknownCurrencyException e) {
            e.printStackTrace();
        }
    }
}
