package uk.co.seamlesssolutions.clients.dlapiper.request;

import java.util.Currency;

public interface IConversionRequest {

    double getAmount();

    Currency getFrom();

    Currency getTo();
}
