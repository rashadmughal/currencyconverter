package uk.co.seamlesssolutions.clients.dlapiper.service;

import uk.co.seamlesssolutions.clients.dlapiper.dao.ICurrencyDao;

public class CurrencyServiceImpl implements ICurrencyService {

    @Override
    public double convert(double amount, ICurrencyDao from, ICurrencyDao to) {
        
        return amount/from.getValueInGbp() * to.getValueInGbp();
    }
}
