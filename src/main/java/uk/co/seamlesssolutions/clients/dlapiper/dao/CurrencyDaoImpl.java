package uk.co.seamlesssolutions.clients.dlapiper.dao;

public class CurrencyDaoImpl implements ICurrencyDao {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private String country;
    private java.util.Currency currencyCode;
    private String currencyName;
    private double valueInGbp;

    public CurrencyDaoImpl(String country, java.util.Currency currencyCode, String currencyName,
            double valueInGbp) {
        this.country = country; 
        this.currencyCode = currencyCode; 
        this.currencyName = currencyName;
        this.valueInGbp = valueInGbp;
    }

    public String getCountry() {
        return country;
    }

    public java.util.Currency getCurrencyCode() {
        return currencyCode;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public double getValueInGbp() {
        return valueInGbp;
    }
    
}
