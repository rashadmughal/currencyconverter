package uk.co.seamlesssolutions.clients.dlapiper.service;

import java.util.Currency;

import uk.co.seamlesssolutions.clients.dlapiper.dao.ICurrencyDao;
import uk.co.seamlesssolutions.clients.dlapiper.exceptions.UnknownCurrencyException;

public interface IExchangeRateService {
    
    ICurrencyDao getCurrency(Currency currencyCode) throws UnknownCurrencyException;
}
